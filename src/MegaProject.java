package ru.denisilin;

import java.math.BigDecimal;
import java.util.ArrayList;

public class MegaProject {
    int p;

    public MegaProject(int p) {
        this.p = p;
    }

    public static void main(String[] args) {
        MegaProject theApp1 = new MegaProject(1);
        MegaProject theApp2 = new MegaProject(2);
        MegaProject theApp3 = new MegaProject(3);

        theApp2.run2();
    }

    public void run2() {
        BigDecimal initialMoney = new BigDecimal(1000);
        BigDecimal currMoney = initialMoney;

        for (int i = 0; i < 300; ++i) {
            currMoney = moneyAfterYear(currMoney);
        }

        System.out.println(currMoney);
    }

    public BigDecimal moneyAfterYear(BigDecimal param) {
        return param.multiply(BigDecimal.valueOf(1.05));
    }

    public void test(Integer[] param) {

    }

    public void run1() {
        System.out.println(p);

        Integer[] test = {1, 2, 3, 5, 6, 9, 10, 11, 13, 18};
        ArrayList<Integer> result = new ArrayList<>();

        //        for (Integer curr : test) {

        for (int i = 0; i < test.length; ++i) {
            Integer curr = test[i];
            if (dividedByTwo(curr)) {
                result.add(curr);
            }
        }

        System.out.println(test);
        System.out.println(result);
    }

    public boolean dividedByTwo(Integer param) {
        return (param % 2) == 0;
    }

    public boolean elementMatch(Integer param) {
        return (param % 3) == 0;
    }
}
